<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	Integer usId = (Integer) session.getAttribute("userId");
	System.out.println(usId);
	if (usId == null || usId == 0) {
		/* 	session.setAttribute("login_msg", "User not logged in.");
			response.sendRedirect("login.jsp"); */
	}
%>



<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Sign Up</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<link rel="stylesheet" type="text/css"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<link href="assets/css/material-kit.css?v=2.0.6" rel="stylesheet" />
<script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
<script src="assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="assets/js/core/bootstrap-material-design.min.js"
	type="text/javascript"></script>
<script src="assets/js/material-kit.js?v=2.0.6" type="text/javascript"></script>

</head>
<body>
	<div class="container">
		<nav
			class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg"
			color-on-scroll="100" id="sectionsNav">
			<div class="container">
				<div class="navbar-translate">
					<!-- <a class="navbar-brand" href="https://demos.creative-tim.com/material-kit/index.html">
          IDEAS SPARK </a> -->
					<p class="font-color">IDEAS SPARK</p>
				</div>
			</div>
		</nav>
	</div>
	<div class="main">
		<div class="container">
			<div class="section text-center">
				<div class="row"></div>
			</div>
		</div>
		<div class="container">
		
			<form class="col-md-6" action="signupServlet" method="POST">

				<div class="input-group">
					<span class="input-group-text"> Title: </span> <select
						name="salutation" class="form-control col-md-2" id="salutations">
						<option value="Mr">Mr.</option>
						<option value="Ms">Ms.</option>
						<option value="Mrs">Mrs.</option>
						<option value="Mdm">Mdm.</option>
					</select>
				</div>

				<br>
				<div class="input-group">
					<span class="input-group-text"> First Name: </span> <input
						class="form-control" name="firstname" size="20" required>
				</div>
				<div class="input-group">
					<span class="input-group-text"> Last Name: </span> <input
						class="form-control" name="lastname" size="20" required>
				</div>
				<div class="input-group">
					<span class="input-group-text"> Email: </span> <input
						class="form-control" type="email" name="email" size="20" required>
				</div>
				<div class="input-group">
					<span class="input-group-text"> Password: </span> <input
						class="form-control" type="password" name="password"
						pattern=".{10,}" required title="10 characters minimum">
				</div>
				<div class="input-group">
					<span class="input-group-text"> Occupation: </span> <input
						class="form-control" type="text" name="occupation" size="20">
				</div>
				<div class="input-group">
					<span class="input-group-text"> Skills: </span> <input
						class="form-control" type="text" name="skills" size="20">
				</div>
				<div class="input-group">
					<span class="input-group-text"> Location: </span> <input
						class="form-control" type="text" name="location" size="20">
				</div>
				<br>
				
				<div class="row">
                <div class="col-md-4 ml-auto mr-auto text-center">
                 <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                
        
              </div>
			</form>

		</div>
		
		<div class="container">
			<div class="section text-center">
				<div class="row"></div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<nav class="float-center">
					<p class="copyright font-color">WKW Internet Programming Group
						II 2019</p>
				</nav>
			</div>
		</footer>
	</div>
</body>
</html>
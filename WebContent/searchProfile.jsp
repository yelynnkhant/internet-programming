<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="database.RetreivePerson"%>
<%@page import="bean.common"%>
<% 
	Integer usId = (Integer)session.getAttribute("userId");
	System.out.println(usId);
	if (usId == null || usId == 0){
		session.setAttribute("login_msg", "User not logged in.");
		response.sendRedirect("login.jsp");
	}
	RetreivePerson conDB = new RetreivePerson(usId); //to reterieve user details
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>View Profile</title>
    <!-- <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/Profile-Edit-Form-1.css">
    <link rel="stylesheet" href="assets/css/Profile-Edit-Form.css">
    <link rel="stylesheet" href="assets/css/styles.css"> -->
    
    <script
	  src="https://code.jquery.com/jquery-3.4.1.min.js"
	  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	  crossorigin="anonymous">
	</script>
    <script> 
    $(function(){
      $("#header").load("head.html"); 
      $("#footer").load("footer.html");
    });
    </script>
</head>
<body>
        <div id="header"></div>
         
        <br/>
        <div class="container container-fluid">
				    <input class="form-control text-dark col-md-3" autofocus type="text" placeholder="Please search here" id="selector" onkeyup="loadProfilelist(this.value)" style="color:white">
        	<div id="table" style="display:none">
		    	<h1>Search Result</h1>
		        <table class="table table-hover">
			    <thead>
			      <tr>
			        <th>Firstname</th>
			        <th>Lastname</th>
			        <th>Email</th>
			        <th></th>
			      </tr>
			    </thead>
			    <tbody id="resultTB">
	
			    </tbody>
			  </table>
		  </div>
		  <div id="table2" style="display:none">
		  </div>
        </div>
        <div id="footer"></div>
        
        
        <script	type ="text/javascript">
       	
        function createRequest() {
        	var req = null;
        	if (XMLHttpRequest) {
        	req = new XMLHttpRequest();
        	}
        	else if (ActiveXObject ) {
        	req = new ActiveXObject("Microsoft.XMLHTTP");
        	}
        	else {
        	req = null;
        	}
        	return req;
        }
        function processXML(responseXML){
			var rsDiv = "";
			var result = responseXML.getElementsByTagName("RESULT")[0];
			for(var i=0; i<result.childNodes.length; i++){
				var person = result.childNodes[i];
				var usID = person.getElementsByTagName("ID")[0];
				var fname = person.getElementsByTagName("FNAME")[0];
				var lname = person.getElementsByTagName("LNAME")[0];
				var email = person.getElementsByTagName("EMAIL")[0];
				var usID = usID.childNodes[0].nodeValue;
				
				rsDiv += "<tr><td>" + fname.childNodes[0].nodeValue + "</td><td>" + lname.childNodes[0].nodeValue + "</td><td>" + email.childNodes[0].nodeValue + "</td><td><a href='viewProfile.jsp?id=" + usID + "'>View Detail</a></td></tr>";
			}
			
			if(rsDiv == ""){
				document.getElementById("table").style.display = "none";
			}
			else{
				document.getElementById("table").style.display = "block";
			}
			document.getElementById("resultTB").innerHTML = rsDiv;
        }
        function sendRequest(url){
        	var request = createRequest();
        	if(request == null){
        		return;
        	}

        	//alert("requ created");
        	request.onreadystatechange = function(){
        		if(request.readyState == 4 && request.status == 200){
        			processXML(request.responseXML);
        		}
        			
        	};
        	request.open("GET", url, true);
        	request.send(null);
        }

       	function loadProfilelist(search){
       		sendRequest("AjaxProfile?searchStr=" + search);
       		//alert("loadstart");
       	}
       	


        </script>
</body>
</html>
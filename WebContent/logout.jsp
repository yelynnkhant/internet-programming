<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <% session.invalidate(); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
</head>
<body>
 <h3>You have been successfully logout !! </h3><br>
 <h4 color="blue">Redirecting to Home page in <span id="lblCount"></span> </h4>
 
 <script type="text/javascript">
 	$(document).ready(function(){
	        var seconds = 3;
	        $("#lblCount").html(seconds);
	        setInterval(function () {
	            seconds--;
	            $("#lblCount").html(seconds);
	            if (seconds == 0) {
	                window.location = "index.jsp";
	            }
	        }, 1000);
	});
</script>
</body>
</html>
<!DOCTYPE html>
<%@page import="java.util.ArrayList"%>
<%@page import="bean.common"%>
<%@page import="database.BrowseDB"%>


<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Ideas Sparks</title>
     <!-- GOOGLE FONTS-->
     <!--JQUERY LIBRARAY-->
    
    <script
	  src="https://code.jquery.com/jquery-3.4.1.min.js"
	  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	  crossorigin="anonymous">
	</script>
    <script> 
    $(function(){
      $("#header").load("head.html"); 
      $("#footer").load("footer.html");
    });
    </script>
</head>
<%@ page import="java.util.*,bean.Ideas" %>
<style>
.footer {
  position: fixed;
  left: 0;
  bottom: 5%;
  width: 100%;
  text-align: center;
}
</style>
<body class="bg-white">
                  
        <div id="header"></div> 
        <div  class="bg-white container container-fluid" >
                    <div class="h3 strong">
                     <h3>Search others' ideas </h3>   
                    </div>             
                 <!-- /. ROW  -->
                <div class="row">
                             <form action="Search" method="POST" class="form form-inline">
                             	<div class="form-group">
	                             <input class="form-control" type="text" id="search-text" name="search-text" 
	                             		onkeyup="sendRequestAuto(this.value);"> 
	                             <input type="submit" value="Search !" class="btn btn-primary"> <br>
	                             </div>
                             </form>
                 </div>
                 
                <strong><div id="myDiv" class="text-primary"> </div></strong>
                 

		 		<div class="row text-center pad-top">
					  <% List<Ideas> ideaList=null;
					  ideaList=new BrowseDB().getAllIdeas();
					  if(ideaList!=null){
					     
					   	 for(int i=0; i<ideaList.size(); i++) { 
					   		 Ideas idea=(Ideas)ideaList.get(i);
					   	 
					   	 %>
		                 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
		                 	  <div class="card card-login text-primary h5 h-100 text-center">
		                      	<b> 
		                      	<% 
		                      	String url=String.format("ideadetail?ideaID=%d", idea.getIdeaID());
		                      	%>
		                      		<a href=<%=url %> ><%= idea.getIdeaName() %></a>
		                 	  	</b> 
		                 	  </div>
		                 </div> 
		               <%} } %>
		        </div> 
		    <div id="footer" class="footer"></div>
		</div>     
     
    
    <script>
    function initRequest(){
    	if(window.XMLHttpRequest){
    		return new XMLHttpRequest();
    	}else if(window.ActiveXObject){
    		return new ActiveXObject("Microsoft.XMLHTTP");
    	}
    }

    function sendRequestAuto(a){
    	//var a = document.getElementById("searchStr");
    	var url = "AjaxServlet?searchStr=" + a;
    	var req = initRequest();
    	req.onreadystatechange = function(){
    		if(req.readyState == 4 && req.status == 200)
    			//alert(req.responseXML);
    			document.getElementById("myDiv").innerHTML = processXML(req.responseXML);
    	};

    	req.open("GET", url, true);
    	req.send(null);
    }

    function processXML(responseXML){
    	var titles = responseXML.getElementsByTagName("Titles")[0];
    	var numTitles = titles.childNodes.length;
    	var divContent ='';
    	if(numTitles > 0){
    		if(numTitles > 3) numTitles = 3; //only shows the first 3
    		for(var i=0; i<numTitles; i++){
    			divContent +=  titles.childNodes[i].childNodes[0].nodeValue + "<br>";
    		}
    	}
    	return divContent;
    }
    </script>

</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<!DOCTYPE html>
<head>
<% 
Integer usId = (Integer)session.getAttribute("userId");
System.out.println(usId);
if (!(usId == null || usId == 0)){
%>
<script>
alert("You have already logged in");
document.location.href = "index.jsp";
</script>
<%
}
%>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Ideas Spark by Group II</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<link rel="stylesheet" type="text/css"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<link href="assets/css/material-kit.css?v=2.0.6" rel="stylesheet" />
<script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
<script src="assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="assets/js/core/bootstrap-material-design.min.js"
	type="text/javascript"></script>
<script src="assets/js/material-kit.js?v=2.0.6" type="text/javascript"></script>
</head>

<body class="login-page sidebar-collapse">
	<nav
		class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg"
		color-on-scroll="100" id="sectionsNav">
		<div class="container">
			<div class="navbar-translate">
				<!-- <a class="navbar-brand" href="https://demos.creative-tim.com/material-kit/index.html">
          IDEAS SPARK </a> -->
				<p class="font-color">IDEAS SPARK</p>
			</div>
		</div>
	</nav>
	<div class="page-header header-filter">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-6 ml-auto mr-auto">
					<div class="card card-login">
						<form class="form" method="post" action="login">
							<div class="card-header card-header-primary text-center">
								<h4 class="card-title">Login</h4>

							</div>

							<br>
							<br>
							<div class="card-body">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text"> <i
											class="material-icons">face</i>
										</span>
									</div>
									<input type="text" class="form-control" placeholder="Username"
										name="username" autofocus="" required="" inputmode="email">
								</div>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text"> <i
											class="material-icons">lock_outline</i>
										</span>
									</div>
									<input type="password" class="form-control"
										placeholder="Password" name="password" required="">
								</div>
								<br/>
								<div class="text-primary">
								<% 
									String login_val = "";
									login_val= (String)session.getAttribute("login_msg");
									System.out.println(login_val+" login val");
									if (login_val != null){
										out.write(login_val);
									} 
								%>						
								</div>
							</div>
							<div class="footer text-center">
								<button class="btn btn-primary btn-link btn-wd btn-lg">Login</button>
								<br> <a href= <%=request.getContextPath() + "/Signup.jsp"%>><u>Sign Up
						</u></a><br>
						<!-- http://localhost:8080/internet-programming/Signup.jsp -->
							</div>
				
						</form>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<nav class="float-center">
					<p class="copyright font-color">WKW Internet Programming Group
						II 2019</p>
				</nav>
			</div>
		</footer>
	</div>
	<!--   Core JS Files   -->

</body>
</html>
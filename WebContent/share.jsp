<!DOCTYPE html>
<%@page import="bean.common"%>
<%@page import="java.util.*" %>
<%@page import="servlets.ShareServlet"%>

<%  //session is set in Login.java. If cannot find userId, sendRedirect to index.html
	Integer usId = (Integer)session.getAttribute("userId");
	System.out.println(usId);
	if (usId == null || usId == 0){
		session.setAttribute("login_msg", "User not logged in.");
		response.sendRedirect("login.jsp");
	}
%>


<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Share Ideas</title>

    
    <script
	  src="https://code.jquery.com/jquery-3.4.1.min.js"
	  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	  crossorigin="anonymous">
	</script>
    <script> 
    $(function(){
      $("#header").load("head.html"); 
      $("#footer").load("footer.html");
    });
    </script>
</head>
<body>
<div id="header"></div>
<div class="container">

<form class="col-md-5" action="ShareServlet" method="POST">
<h3>I would like to see on the market:</h3>
  <div class="form-group">
    <input type="text" class="form-control" name="ideaName" placeholder="Enter Idea" required pattern=".*\S+.*" title="This field is required" maxlength = "50">
  </div>
  <div class="form-group">
    <textarea class="form-control" name="description" rows="3" placeholder="Enter Problem Description" required pattern=".*\S+.*" title="This field is required" maxlength = "1200"></textarea>
  </div>
   <div class="form-group">
    <input type="text" class="form-control" name="tagName" placeholder="Enter Tag" required pattern=".*\S+.*" title="This field is required" maxlength = "50">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
  <a class="btn btn-danger" href="index.jsp" role="button">Browse</a>
</form>
</div>
<div id="footer"></div>


<!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="<%=common.url %>assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="<%=common.url %>assets/js/bootstrap.min.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="<%=common.url %>assets/js/custom.js"></script>

</body>
</html>
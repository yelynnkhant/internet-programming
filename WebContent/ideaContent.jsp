<%@page import="bean.Comments"%>
<%@page import="bean.Ideas"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="bean.common"%>
<%@page import="database.BrowseDB"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="bean.SimilarProjects"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Ideas Spark by Group II</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<link rel="stylesheet" type="text/css"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<link href="assets/css/material-kit.css?v=2.0.6" rel="stylesheet" />
<script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
<script src="assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="assets/js/core/bootstrap-material-design.min.js"
	type="text/javascript"></script>
<script src="assets/js/material-kit.js?v=2.0.6" type="text/javascript"></script>
<script>
	$(function() {
		$("#header").load("head.html");
		$("#footer").load("footer.html");
	});
	$('#commentModal').on('hidden', function() {
		var commentTextarea = document.getElementById("commentTextarea");
		var commentForm = document.getElementById('commentForm');
		commentForm.classList.remove('has-danger');
		commentTextarea.val('');
	});
	function initRequest() {
		if (window.XMLHttpRequest) {
			return new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			isIE = true;
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	function sendRequest() {
		var url = "comment"
		var ideaIDInput = document.getElementById("ideaID");
		var ideaID = encodeURIComponent(ideaIDInput.value.trim());
		console.log(ideaID)
		var commentTextarea = document.getElementById("commentTextarea");
		var commentForm = document.getElementById('commentForm');
		if (commentTextarea.value.trim() == '') {
			commentForm.classList.add('has-danger');
			commentTextarea.focus();
			return;
		}
		var comment = encodeURIComponent(commentTextarea.value.trim());
		console.log(comment)
		var param = 'ideaID=' + ideaID + '&comment=' + comment

		var req = initRequest();
		req.onreadystatechange = function() {
			console.log(req.status)
			if (req.readyState == 4) {
				if (req.status == 200) {
					console.log("success!")
					document.getElementById("table").innerHTML = processXML(req.responseXML);
					$('#commentModal').modal('hide');

				} else if (req.status == 404) {
					//clearTable();
					window.location = "login.jsp"
				}
			}
		}
		req.open("POST", url, true);
		req.setRequestHeader('Content-type',
				'application/x-www-form-urlencoded');
		req.send(param);
	}
	function processXML(responseXML) {
		console.log(responseXML)
		var comments = responseXML.getElementsByTagName("Comments")[0];
		var numComments = comments.childNodes.length;
		var commentsTable = '';
		if (numComments > 0) {
			commentsTable += '<table id="table" class="table">';
			commentsTable += '<thead></thead>';
			commentsTable += '<tbody>';
			for (i = 0; i < numComments; i++) {
				var aComment = comments.childNodes[i];
				var commentContent = aComment.getElementsByTagName("Content")[0];
				var commentUser = aComment.getElementsByTagName("User")[0];
				var commentDate = aComment.getElementsByTagName("Date")[0];

				commentsTable += '<tr>';
				commentsTable += '<td>'
						+ commentContent.childNodes[0].nodeValue + '<td/>';
				commentsTable += '<td> Commented by '
						+ commentUser.childNodes[0].nodeValue + '<td/>';
				commentsTable += '<td> on '
						+ commentDate.childNodes[0].nodeValue + '<td/>';
				commentsTable += '</tr>';
			}
			commentsTable += '</tbody>';
			commentsTable += '</table>';
			return commentsTable;
		}
	}
</script>
</head>

<body>
	<div id="header"></div>

	<!-- /. NAV SIDE  -->
	<div class="container">
		<br>
		<div class="card card-nav-tabs">
			<div class="card-header card-header-warning">Idea</div>
			<div class="card-body">
				<h4 class="card-title">

					<%
						Ideas idea = (Ideas) request.getAttribute("ideaObj");
						String ideaID = idea.getIdeaID().toString();
						String ideaName = idea.getIdeaName();
					%>

					<%=ideaName%>
				</h4>
				<p class="card-text">
					<%=idea.getDescription()%>
				</p>
				<!-- To-do -->
				<span class="badge badge-info" onclick="future()"><%=new BrowseDB().searchForTag(ideaName, true)%></span>


			</div>
		</div>
		<br>



		<!-- Comment -->
		<div class="card card-nav-tabs">
			<h4 class="card-header card-header-info">Comments</h4>
			<div class="card-body text-right">
				<button type="button" class="btn btn-primary btn-round"
					data-toggle="modal" data-target="#commentModal">Add
					Comment</button>
			</div>
			<div class="card-body">
				<%
					List<Comments> commentList = null;
					commentList = (List) request.getAttribute("comments");
					System.out.print(commentList);
					if (commentList != null && commentList.size() != 0) {
						SimpleDateFormat format = new SimpleDateFormat("MMM dd yyyy");
				%>
				<table id="table" class="table">
					<thead></thead>
					<tbody>
						<%
							for (int i = 0; i < commentList.size(); i++) {
									Comments comment = (Comments) commentList.get(i);
						%>
						<tr>
							<td><%=comment.getComment()%></td>
							<td>Commented by <%=comment.getSalutations()%> <%=comment.getLastName()%></td>
							<td>on <%=format.format(comment.getDatePosted())%></td>
						</tr>
						<%
							}
						%>

					</tbody>
				</table>
				<%
					} else {
						System.out.println("no comment");
				%>
				<h4 class="card-title">No Comment</h4>
				<%
					}
				%>
			</div>
		</div>


		<!-- Modal -->
		<div class="modal fade" id="commentModal" tabindex="-1" role="dialog"
			aria-labelledby="commentModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="commentModalLabel">Add Comment</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form id="form" class="form">
						<input id="ideaID" name="ideaID" value=<%=ideaID%> type="hidden">
						<div class="modal-body">
							<div class="card-body">
								<div id="commentForm" class="form-group">
									<label for="commentControlTextarea" class="control-label">Please
										fill in your comment:</label>
									<textarea class="form-control" id="commentTextarea"
										name="commentTextarea" rows="3"></textarea>
								</div>
							</div>

						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-secondary"
								data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary"
								onClick="sendRequest();">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<br>
		<div class="card card-nav-tabs">
			<h4 class="card-header card-header-success">Similar Projects</h4>
			<div class="card-body text-right">

				<div class="float-right">
					<%
						Integer usId = (Integer) session.getAttribute("userId");
						if (usId == null || usId == 0) {
							session.setAttribute("modal_type", 1);
					%>
					<button class="btn btn-primary btn-round" data-toggle="modal"
						data-target="#myUserModal">Share Similar Project</button>
					<%
						} else {
							session.setAttribute("modal_type", 2);
					%>
					<button class="btn btn-primary btn-round" data-toggle="modal"
						data-target="#myModal">Share Similar Project</button>
					<%
						}
					%>

				</div>
			</div>
			<div class="card-body">

				<%
				List<SimilarProjects> similarProjects1 = (ArrayList<SimilarProjects>) request
				.getAttribute("similarProjectsFromServlet");
				if(similarProjects1.isEmpty()){%>
				<h4 class="card-title">No Similar Project</h4>
				<%}else{ %>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>Title</th>
								<th>Description</th>
								<th>Url</th>
							</tr>
						</thead>
						<tbody>
							<%
								for (SimilarProjects similarProject : similarProjects1) {
							%>
							<tr>

								<td><%=similarProject.getTitleName()%> <br> Shared By
									<%=similarProject.getUser().getUserLastName()%></td>
								<td><%=similarProject.getDescription()%></td>
								<td><a href=<%=similarProject.getUrl()%>><u><%=similarProject.getUrl()%>
									</u></a></td>
							</tr>

							<%
								}
							%>
						</tbody>
					</table>
				</div>
				<%} %>
			</div>
		</div>
	</div>



	<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title">Share Similar Project</h3>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<i class="material-icons">clear</i>
					</button>
				</div>
				<div class="modal-body">
					<form action="IdeaDetailServlet" method="post">
						<input name="ideaID" type="hidden" value=<%=ideaID%> />
						<div class="input-group">
							<span class="input-group-text"> Title: </span> <input
								name="title_name" class="form-control"
								required="Please Fill Title">
						</div>
						<div class="input-group">
							<span class="input-group-text">URL: </span> <input name="url"
								class="form-control" required="Please Fill URL">
						</div>
						<div class="input-group">
							<span class="input-group-text"> Description: </span>
							<textarea class="form-control" name="description" rows="3"
								name="description"></textarea>
						</div>
						<div class="modal-footer">
							<input type="submit" class="btn btn-primary btn-round"
								value="Save">
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>

	<div class="modal fade" id="myUserModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">

					<h3>
						<b>Oops! Sorry....</b>
					</h3>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<i class="material-icons">clear</i>
					</button>

				</div>
				<div class="modal-body">
					<br>
					<h3 class="modal-title">
						You haven't logged in. <b>Please Login and Come Back!</b>
					</h3>
					<br> <br>
				</div>
			</div>

		</div>
	</div>
	<div id="footer"></div>

	<script type="text/javascript">
		function future() {
			alert("This is our future function.");
		}
	</script>
</body>
</html>
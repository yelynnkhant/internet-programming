<%@page import="database.RetreivePerson"%>
<%@page import="bean.common"%>
<% 
	Integer usId = (Integer)session.getAttribute("userId");
	System.out.println(usId);
	if (usId == null || usId == 0){
		session.setAttribute("login_msg", "User not logged in.");
		response.sendRedirect("login.jsp");
	}
	RetreivePerson conDB = new RetreivePerson(usId); //to reterieve user details
%>



<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Edit Profile</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/Profile-Edit-Form-1.css">
    <link rel="stylesheet" href="assets/css/Profile-Edit-Form.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    
    <script
	  src="https://code.jquery.com/jquery-3.4.1.min.js"
	  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	  crossorigin="anonymous">
	</script>
    <script> 
    $(function(){
      $("#header").load("head.html"); 
    });
    </script>
</head>

<body>
	<div id="header"></div>
         
    <div class="container profile profile-view" id="profile">
            <form action="updateUser" method="POST">
            <div class="form-row profile-row">
                <div class="col-md-8">
                <div class="form-row">
                            <h1>Profile </h1>
                            <div class="col-md-4">
	        					<% if(request.getParameter("info") != null){ %>
				              	<div class="alert alert-success" role="alert">
				              	<%= request.getParameter("info")  %>
								</div>
								<% } %>
        					</div>
                </div>
 
                    <hr>
                    <div class="form-row">
                        <div class="col-sm-12 col-md-2">
                        
	                        <div class="form-group">
	                        	<label>Title</label>
		                        <select name="title" class="form-control" id="salutations">
								  <option value="Mr">Mr.</option>
								  <option value="Ms">Ms.</option>
								  <option value="Mrs">Mrs.</option>
								  <option value="Mdm">Mdm.</option>
								</select>
							</div>
                        
                            <!-- <div class="form-group"><label>Title</label><input class="form-control" type="text" name="title" value="<%= conDB.getTitle() %>"></div> -->
                        </div>
                       <div class="col-sm-12 col-md-5">
                           <label>First Name</label><input class="form-control" type="text" name="firstname" value="<%= conDB.getFName() %>">
                       </div>
                       <div class="col-sm-12 col-md-5">
                           <label>Last Name</label><input class="form-control" type="text" name="lastname" value="<%= conDB.getLName() %>">
                          </div>
                          
               
                    </div>
                    <div class="form-group"><label>Email </label><input class="form-control" type="email" autocomplete="off" required="" name="email" value="<%= conDB.getEmail() %>"></div>
                    <div class="form-row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group"><label>Skills</label><input class="form-control" type="text" name="skills" autocomplete="off" required="" value="<%= conDB.getSkills() %>"></div>
                            <div class="form-group"><label>Country</label><input class="form-control" type="text" name="country" autocomplete="off" required="" value="<%= conDB.getCountry() %>"></div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group"><label>Occupation</label><input class="form-control" type="text" name="occupation" autocomplete="off" required="" value="<%= conDB.getOccupation() %>"></div>
                            <div class="form-group"><label>Password</label><input class="form-control" type="password" placeholder="Leave to use the current password" name="password" autocomplete="off"></div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="col-md-10 content-right"><input class="btn btn-danger" type="reset" value="Reset"></div>
                        <div class="col-md-2 content-right"><input class="btn btn-primary" type="submit" value="Save"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    	var salu = document.getElementById("salutations");
    	for(var i=0; i<4; i++){
    		if( salu.options[i].value == "<%= conDB.getTitle() %>"){
        		salu.options[i].selected = true;        		
        		break;
    		}
    	}
    </script>
</body>

</html>
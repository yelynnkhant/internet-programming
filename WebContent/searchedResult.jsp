<!DOCTYPE html>
<%@page import="bean.common"%>
<%@page import="database.BrowseDB"%>
<%@page import="servlets.Search"%>
<%@page import="java.util.*"%>
<%@page import="bean.Ideas"%>
<%
	/*
	Integer usId = (Integer)session.getAttribute("userId");
	System.out.println(usId);
	if (usId == null || usId == 0){
		session.setAttribute("login_msg", "User not logged in.");
		response.sendRedirect("login.jsp");
	}
	*/
%>

<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Ideas Spark</title>
<!-- GOOGLE FONTS-->
<!--JQUERY LIBRARAY-->

<script src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous">
	
</script>
<script>
	$(function() {
		$("#header").load("head.html");
		$("#footer").load("footer.html");
	});
</script>
</head>

<body class="bg-white">

	<div id="header"></div>

	<div class="container-fluid">
		<div class="h3 strong">
			<h3>Search others' ideas</h3>
		</div>
		<!-- /. ROW  -->
		<div class="row">
			<form action="Search" method="POST" class="form form-inline">
				<div class="form-group">
					<input class="form-control" type="text" id="search-text"
						name="search-text" onkeyup="sendRequestAuto(this.value);">
					<input type="submit" value="Search !" class="btn btn-primary">
					<br>
				</div>
			</form>
		</div>

		<div id="myDiv" class="text-primary"></div>

		<div class="row text-center pad-top">
			<%
				List<Ideas> ideaList = null;
				ideaList = (List) request.getAttribute("ideaList");
				if (ideaList != null) {

					for (int i = 0; i < ideaList.size(); i++) {
						Ideas idea = (Ideas) ideaList.get(i);
			%>
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
				<div class="card card-login text-primary h5 h-100 text-center">
					<%
						String url = String.format("ideadetail?ideaID=%d", idea.getIdeaID());
					%>
					<a href=<%=url%>><%=idea.getIdeaName()%></a>
				</div>
			</div>
			<%
				}
				}
			%>
		</div>
		<div id="footer"></div>
	</div>

	<script>
		function initRequest() {
			if (window.XMLHttpRequest) {
				return new XMLHttpRequest();
			} else if (window.ActiveXObject) {
				return new ActiveXObject("Microsoft.XMLHTTP");
			}
		}

		function sendRequestAuto(a) {
			//var a = document.getElementById("searchStr");
			var url = "AjaxServlet?searchStr=" + a;
			var req = initRequest();
			req.onreadystatechange = function() {
				if (req.readyState == 4 && req.status == 200)
					//alert(req.responseXML);
					document.getElementById("myDiv").innerHTML = processXML(req.responseXML);
			};

			req.open("GET", url, true);
			req.send(null);
		}

		function processXML(responseXML) {
			var titles = responseXML.getElementsByTagName("Titles")[0];
			var numTitles = titles.childNodes.length;
			var divContent = '';
			if (numTitles > 0) {
				if (numTitles > 3)
					numTitles = 3; //only shows the first 3
				for (var i = 0; i < numTitles; i++) {
					divContent += titles.childNodes[i].childNodes[0].nodeValue
							+ "<br>";
				}
			}
			return divContent;
		}
	</script>

</body>
</html>

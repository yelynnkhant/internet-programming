<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="database.RetreivePerson"%>
<%@page import="bean.common"%>

<% 
Integer usId = Integer.parseInt(request.getParameter("id")); //get id from GET 
if (usId == null || usId == 0){
	session.setAttribute("login_msg", "User not logged in.");
	response.sendRedirect("login.jsp");
}
	RetreivePerson person = new RetreivePerson(usId);        
%>
        
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>View Profile</title>
    <!-- <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/Profile-Edit-Form-1.css">
    <link rel="stylesheet" href="assets/css/Profile-Edit-Form.css">
    <link rel="stylesheet" href="assets/css/styles.css"> -->
    <title>Profile Detail</title>
    <script
	  src="https://code.jquery.com/jquery-3.4.1.min.js"
	  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	  crossorigin="anonymous">
	</script>
    <script> 
    $(function(){
      $("#header").load("head.html"); 
      $("#footer").load("footer.html");
    });
    </script>
</head>
<body>
		<div id="header"></div>
        
        <br/>
        <div class="container container-fluid">
        	<div id="table">
		    	<h1>
		    	</h1>
		        <table class="table table-hover">
		          <thead>
		          	<th class="text-left h2" colspan="2"><%= person.getFName() %> <%= person.getLName() %></th>
		          </thead>
			      <tr>
			      	<th>Email</th>
			      	<td><%= person.getEmail() %></td>
			      </tr>
			      <tr>
			      	<th>Skills</th>
			      	<td><%= person.getSkills() %></td>
			      </tr>
			      <tr>
			      	<th>Occupation</th>
			      	<td><%= person.getOccupation() %></td>
			      </tr>
			      <tr>
			      	<th>Country</th>
			      	<td><%= person.getCountry() %></td>
			      </tr>
			      <tr>
			      	<th>Posted Ideas</th>
			      	<td><%= person.getIdeas() %></td>
			      </tr>
			    <tbody id="resultTB">
	
			    </tbody>
			  </table>
		  </div>
		  <div id="table2" style="display:none">
		  </div>
        </div>
		<div id="footer"></div>
	
</body>
</html>
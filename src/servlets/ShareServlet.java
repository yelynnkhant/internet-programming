package servlets;

import java.io.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import database.ShareDBAO;
import bean.Ideas;
import bean.Tags;


/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/ShareServlet")
public class ShareServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShareServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
				String ideaName = request.getParameter("ideaName");
				String description = request.getParameter("description");
				String tagName = request.getParameter("tagName");
				
				HttpSession session = request.getSession(true);
				Integer userid = (Integer) session.getAttribute("userId");

				Ideas Ideas = new Ideas();
				Tags Tags = new Tags();
				Ideas.setIdeaName(ideaName);
				Ideas.setDescription(description);
				Tags.setTagName(tagName);
				Ideas.setUserID(userid);
				boolean result = false ;
				
				try {
					ShareDBAO share = new ShareDBAO();
					Boolean shareValidate = share.postIdeas(Ideas,Tags);
					if(shareValidate) {
						request.getRequestDispatcher("index.jsp").forward(request, response);
					}
				}
				catch (Exception e)
				{
				 e.printStackTrace();					
				}		

	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
}
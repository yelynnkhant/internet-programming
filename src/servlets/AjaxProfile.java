package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.searchProfile;

/**
 * Servlet implementation class AjaxProfile
 */
@WebServlet("/AjaxProfile")
public class AjaxProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjaxProfile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		PrintWriter out = response.getWriter();
	
		String str = request.getParameter("searchStr");
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		if(!str.equals("")) {
			Vector<String> nameList = new searchProfile().searchInDBAjax(str);
			System.out.println(nameList);
			StringBuilder sbXML = new StringBuilder();
			for(int i=0; i<nameList.size();i++) {
				sbXML.append("<PERSON>");
				String rawStr[] = (nameList.get(i)).split(",,");
				String usID = rawStr[0];
				String fname = rawStr[1];
				String lname = rawStr[2];
				String email = rawStr[3];
				
				sbXML.append("<ID>");
				sbXML.append(usID);
				sbXML.append("</ID>");
				
				sbXML.append("<FNAME>");
				sbXML.append(fname);
				sbXML.append("</FNAME>");
				
				sbXML.append("<LNAME>");
				sbXML.append(lname);
				sbXML.append("</LNAME>");
				
				sbXML.append("<EMAIL>");
				sbXML.append(email);
				sbXML.append("</EMAIL>");
				
				sbXML.append("</PERSON>");
			}
			System.out.println(sbXML.toString());
			out.write("<RESULT>" + sbXML.toString()  +"</RESULT>");
		}
		else {
			out.write("<RESULT>" +""+"</RESULT>");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

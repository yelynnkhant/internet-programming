package servlets;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Users;
import database.DBConnection;

//import com.sun.jmx.snmp.Timestamp;


/**
 * Servlet implementation class signupServlet
 */
@WebServlet("/signupServlet")
public class signupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;	
	private Connection con;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		//String id = request.getParameter("id");
		String salutation =  request.getParameter("salutation");
		String firstname = request.getParameter("firstname");
		String lastname =  request.getParameter("lastname");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String occupation = request.getParameter("occupation");
		String skills = request.getParameter("skills");
		String location = request.getParameter("location");
		Timestamp dateregistered = new Timestamp(System.currentTimeMillis());
		
		byte[] hash = null ;
		MessageDigest digest;
		String salt = password + "Spark";
		 StringBuilder sb = new StringBuilder();
		try {
		digest = MessageDigest.getInstance("SHA-256");
			hash = digest.digest(salt.getBytes(StandardCharsets.UTF_8));
		        for (byte b : hash) {
		            sb.append(String.format("%02x", b));
		        }

		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		//DBAO account = new DBAO();
		//result = account.authenticate(id, password);		
	
		con = DBConnection.createConnection();
		try {		
		PreparedStatement ps;

			String query = "INSERT INTO users (salutations, firstname, lastname, email, password, occupation, skills, location, dateregistered)"
					+ " VALUES (?,?,?,?,?,?,?,?,?);";
	    	ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
	    	ps.setString(1, salutation);
	    	ps.setString(2, firstname);
	    	ps.setString(3, lastname);
	    	ps.setString(4, email);
	    	ps.setString(5, sb.toString());
	    	ps.setString(6, occupation);	
	    	ps.setString(7, skills);
	    	ps.setString(8, location);
	    	ps.setTimestamp(9, dateregistered);

	    	System.out.println(ps);
	    	ps.executeUpdate();
	    	ResultSet generatedPsKeys = ps.getGeneratedKeys();
			generatedPsKeys.next();
			int id = generatedPsKeys.getInt(1);
	    	System.out.println("updated");
	    	request.getSession(true).setAttribute("userId", id);
	    	
	    	response.sendRedirect("index.jsp");
			}
		catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) con.close();
			} catch(Exception e) {
    		e.printStackTrace();
    	}
		}
	}

}

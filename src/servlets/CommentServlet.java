package servlets;

import java.awt.print.Book;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.org.apache.bcel.internal.generic.NEW;

import bean.Comments;
import database.CommentDBAO;

/**
 * Servlet implementation class CommentServlet
 */
@WebServlet("/CommentServlet")
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CommentServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		PrintWriter out = response.getWriter();

		String commentText = request.getParameter("comment");
		String ideaID = request.getParameter("ideaID");
		System.out.print(commentText);
		System.out.print(ideaID);
		HttpSession session = request.getSession(false);
		System.out.println(session.getAttribute("userID"));
		if (session.getAttribute("userId") != null) {
			System.out.println("Hello");

			Integer userID = (Integer) session.getAttribute("userId");
			System.out.println(userID);

			List commentList = null;
			try {
				CommentDBAO commentDBAO = new CommentDBAO();
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				Comments commentObj = new Comments(commentText, timestamp, userID, Integer.valueOf(ideaID));
				System.out.print(commentObj);
				commentDBAO.addComment(commentObj);

				commentList = commentDBAO.getComments(ideaID);
				SimpleDateFormat simpleDateFormat=new SimpleDateFormat("MMM dd yyyy");

				if (commentList != null && commentList.size() != 0) {
					StringBuilder sbXML = new StringBuilder();
					for (int i = 0; i < commentList.size(); i++) {
						Comments comment = (Comments) commentList.get(i);
						sbXML.append("<Comment>");
						sbXML.append("<Content>" + comment.getComment() + "</Content>");
						sbXML.append("<User>" + comment.getSalutations()+" "+comment.getLastName() + "</User>");
						sbXML.append("<Date>" + simpleDateFormat.format(comment.getDatePosted()) + "</Date>");
						sbXML.append("</Comment>");

					}

					out.write("<Comments>" + sbXML.toString() + "</Comments>");

				}
			} catch (Exception e) {

				response.setStatus(HttpServletResponse.SC_NO_CONTENT);
				response.resetBuffer();
				throw new ServletException(e);
			}
		} else {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			response.resetBuffer();
		}

	}

}

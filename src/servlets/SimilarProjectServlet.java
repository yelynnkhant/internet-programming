package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.SimilarProjects;
import database.SimilarProjectDAO;

/**
 * Servlet implementation class SimilarProjectServlet
 */
@WebServlet("/SimilarProjectServlet")
public class SimilarProjectServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private SimilarProjectDAO similarProjectDAO = new SimilarProjectDAO();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SimilarProjectServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String ideaName = request.getParameter("ideaTitle").toString();
		char[] charArr =  ideaName.toCharArray();
		String output = "";
		for(int i=0; i<ideaName.length(); i++){
			if(charArr[i] == '%' && charArr[i+1] == '2' && charArr[i+2] == '0'){
				output += ' ';
				i= i+2;
			}else{
            	output += charArr[i];
                }
		}

		ideaName = output;
		SimilarProjectDAO similarProjectDAO;
		try {
			similarProjectDAO = new SimilarProjectDAO();
			String ideaID = request.getParameter("ideaID");
//			request.getSession(false).setAttribute("idea-name-for-sp", ideaName);
			List<SimilarProjects> projectList = similarProjectDAO.getSimilarProjectsByIdeaID(ideaID);
			request.setAttribute("similarProjectsFromServlet", projectList);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/ideaContent.jsp");

			requestDispatcher.forward(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String ideaName = (String) request.getSession(false).getAttribute("idea-name-for-sp");
		Integer model_type = (Integer)request.getSession(false).getAttribute("modal_type");
		if (model_type == 2) {
			String titleName = request.getParameter("title_name");
			String url = request.getParameter("url");
			String description = request.getParameter("description");

			Integer ideaID = similarProjectDAO.findIdeaIdByname(ideaName);
			Integer userID = (Integer) request.getSession(false).getAttribute("userId");
			SimilarProjects similarProject = new SimilarProjects(titleName, url, description, userID, ideaID);
			try {
				boolean status = similarProjectDAO.insertSimilarProject(similarProject);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		

		// show similar projects

		SimilarProjectDAO similarProjectDAO;
		try {
			similarProjectDAO = new SimilarProjectDAO();
			List<SimilarProjects> projectList = similarProjectDAO.getSimilarProjectsByIdeaID(ideaName);
			request.setAttribute("similarProjectsFromServlet", projectList);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/ideaContent.jsp");

			requestDispatcher.forward(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

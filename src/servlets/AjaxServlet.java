package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.BrowseDB;

/**
 * Servlet implementation class AjaxServlet
 */
@WebServlet("/AjaxServlet")
public class AjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		PrintWriter out = response.getWriter();
	
		String str = request.getParameter("searchStr");
		if(!str.equals("")) {
			Vector<String> titleList = new BrowseDB().searchInDBAjax(str);
			System.out.println(titleList);
			StringBuilder sbXML = new StringBuilder();
			for(int i=0; i<titleList.size();i++) {
				sbXML.append("<Title>");
				sbXML.append(titleList.get(i));
				sbXML.append("</Title>");
			}
			System.out.println("<Titles>" + sbXML.toString()  +"</Titles>");
			out.write("<Titles>" + sbXML.toString()  +"</Titles>");
		}
		else {
			out.write("<Titles>" +""+"</Titles>");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

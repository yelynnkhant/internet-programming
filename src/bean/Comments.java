package bean;

import java.io.Serializable;
import java.sql.Timestamp;

public class Comments implements Comparable<Object> {
	private Integer commentID;
	private String comment;
	private Timestamp datePosted;
	private Integer userID;
	private Integer ideaID;
	private String lastName;
	private String salutations;

	public Comments() {
	}

	public Comments(String comment, Timestamp createDate, Integer userID, Integer ideaID) {
		this.comment = comment;
		this.userID = userID;
		this.ideaID = ideaID;
		this.datePosted = createDate;

	}

	public Integer getCommentID() {
		return commentID;
	}

	public void setCommentID(Integer commentID) {
		this.commentID = commentID;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Timestamp getDatePosted() {
		return datePosted;
	}

	public void setDatePosted(Timestamp datePosted) {
		this.datePosted = datePosted;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public Integer getIdeaID() {
		return ideaID;
	}

	public void setIdeaID(Integer ideaID) {
		this.ideaID = ideaID;
	}

	public int compareTo(Object o) {
		Comments comment = (Comments) o;
		int lastCmp = comment.datePosted.compareTo(datePosted);
		return lastCmp;

	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSalutations() {
		return salutations;
	}

	public void setSalutations(String salutations) {
		this.salutations = salutations;
	}


}

package bean;

import java.sql.Date;
import java.sql.Timestamp;

public class Ideas {
	private Integer ideaID;
	private String ideaName;
	private String description;
	private Date datePublished;
	private Integer userID;
	
	public Ideas() {
		
	}

	public Ideas(Integer ideaID, String ideaName, String description, Date datePublished, Integer userID) {
		super();
		this.ideaID = ideaID;
		this.ideaName = ideaName;
		this.description = description;
		this.datePublished = datePublished;
		this.userID = userID;
	}
	public Integer getIdeaID() {
		return ideaID;
	}
	public void setIdeaID(Integer ideaID) {
		this.ideaID = ideaID;
	}
	public String getIdeaName() {
		return ideaName;
	}
	public void setIdeaName(String ideaName) {
		this.ideaName = ideaName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDatePublished() {
		return datePublished;
	}
	public void setDatePublished(Date datePublished) {
		this.datePublished = datePublished;
	}
	public Integer getUserID() {
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}
}

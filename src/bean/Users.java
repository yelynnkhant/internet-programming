package bean;

import java.sql.Timestamp;

public class Users {
	private Integer userID;
	private String salutations;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String occupation;
	private String skills;
	private String location;
	private Timestamp dateRegistered;
	
	public Users() {
		this.userID = 0;
		this.salutations = "";
		this.firstName = "";
		this.lastName = "";
		this.email = "";
		this.occupation = "";
		this.skills = "";
		this.location = "";
		this.dateRegistered = new Timestamp(userID);
	}
	
	public Users(int userID,String salutations,String firstName,String lastName,String email,String occupation,String skills,String location,Timestamp dateRegistered) {
		this.userID = userID;
		this.salutations = salutations;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.occupation = occupation;
		this.skills = skills;
		this.location = location;
		this.dateRegistered = dateRegistered;
	}


	public Users(String salutations, String firstName, String lastName, String email, String password,
			String occupation, String skills, String location) {
		super();
		this.salutations = salutations;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.occupation = occupation;
		this.skills = skills;
		this.location = location;
	}

	public Integer getUserID() {
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	public String getSalutations() {
		return salutations;
	}
	public void setSalutations(String salutations) {
		this.salutations = salutations;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getSkills() {
		return skills;
	}
	public void setSkills(String skills) {
		this.skills = skills;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Timestamp getDateRegistered() {
		return dateRegistered;
	}
	public void setDateRegistered(Timestamp dateRegistered) {
		this.dateRegistered = dateRegistered;
	}
	
	public String getUserLastName() {
		return this.salutations + " " + this.lastName;
	}
}

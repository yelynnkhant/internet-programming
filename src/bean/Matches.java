package bean;

public class Matches {
	private Integer matchesID;
	private Integer ideaID;
	private Integer tagID;
	
	public Integer getMatchesID() {
		return matchesID;
	}
	public void setMatchesID(Integer matchesID) {
		this.matchesID = matchesID;
	}
	public Integer getIdeaID() {
		return ideaID;
	}
	public void setIdeaID(Integer ideaID) {
		this.ideaID = ideaID;
	}
	public Integer getTagID() {
		return tagID;
	}
	public void setTagID(Integer tagID) {
		this.tagID = tagID;
	}
	

}

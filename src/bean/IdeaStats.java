package bean;

public class IdeaStats {
	private Integer ideaStatsID;
	private Integer userID;
	private Integer ideaID;
	public Integer getIdeaStatsID() {
		return ideaStatsID;
	}
	public void setIdeaStatsID(Integer ideaStatsID) {
		this.ideaStatsID = ideaStatsID;
	}
	public Integer getUserID() {
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	public Integer getIdeaID() {
		return ideaID;
	}
	public void setIdeaID(Integer ideaID) {
		this.ideaID = ideaID;
	}
	 
}

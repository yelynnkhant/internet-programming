package bean;

import java.io.Serializable;
import java.sql.Timestamp;

public class SimilarProjects implements Serializable{
	private Integer titleID;
	private String titleName;
	private String url;
	private String description;
	private Timestamp dataPosted;
	private Integer userID;
	private Integer ideaID;	
	private Users user;
	
	public SimilarProjects() {
	}
	public SimilarProjects(String titleName, String url, String description, Integer userID,
			Integer ideaID) {
		super();
		this.titleName = titleName;
		this.url = url;
		this.description = description;
		this.userID = userID;
		this.ideaID = ideaID;
		this.dataPosted = new Timestamp(System.currentTimeMillis());
		user = new Users();
	}
	public Integer getTitleID() {
		return titleID;
	}
	public void setTitleID(Integer titleID) {
		this.titleID = titleID;
	}
	public String getTitleName() {
		return titleName;
	}
	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Timestamp getDataPosted() {
		return dataPosted;
	}
	public void setDataPosted(Timestamp dataPosted) {
		this.dataPosted = dataPosted;
	}
	public Integer getUserID() {
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	public Integer getIdeaID() {
		return ideaID;
	}
	public void setIdeaID(Integer ideaID) {
		this.ideaID = ideaID;
	}
	public Users getUser() {
		return user;
	}
	public void setUser(Users user) {
		this.user = user;
	}
	
	public String getTimeDiff() {
		String result = "";
		
		return "";
	}
	

}

package database;

import java.sql.*;
import java.util.*;
import database.DBConnection;
import bean.Ideas;
import bean.Tags;
import bean.Matches;

public class ShareDBAO {

	public Integer checkTagNameExists(Tags Tag) {

		Integer tagID = 0;
		Connection con = null;
		try {
			con = DBConnection.createConnection();
			String tagname = Tag.getTagName();
			String getTagID = "select tagID from tags where tagname=?";

			PreparedStatement ps4;
			ps4 = con.prepareStatement(getTagID);
			ps4.setString(1, tagname);
			ResultSet resultSet = ps4.executeQuery();

			if (resultSet.next()) {
				tagID = resultSet.getInt("tagID");
			} else {
				tagID = 0;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
    		try {
    			if(con != null) con.close();
    		}
        	catch(Exception e) {
        		e.printStackTrace();
        	}
    	}
		return tagID;
	}

	public Boolean postIdeas(Ideas Ideas, Tags Tags) {

		String ideaName = Ideas.getIdeaName();
		String description = Ideas.getDescription();
		String tags = Tags.getTagName();
		Integer userID = Ideas.getUserID();
		Integer ideaID = 0;
		Integer tagID = 0;
		Integer matchesID = 0;
		Connection con = null;
		Boolean status = false;
		Integer test = 1; // test without session
		Matches matches = new Matches();

		con = DBConnection.createConnection();

		try {

			Calendar calendar = Calendar.getInstance(); // create a sql date object
			java.sql.Date datepublished = new java.sql.Date(calendar.getTime().getTime());
			String insertidea = "INSERT INTO Ideas (ideaid, ideaName, description, datepublished, userID) VALUES (?, ?, ?, ?, ?)";
			PreparedStatement ps;
			ps = con.prepareStatement(insertidea, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, ideaID);
			ps.setString(2, ideaName);
			ps.setString(3, description);
			ps.setDate(4, datepublished);
			// ps.setInt(5, test); // test without session
			ps.setInt(5, userID);
			ps.executeUpdate();
			ResultSet generatedPsKeys = ps.getGeneratedKeys();
			generatedPsKeys.next();
			int matchesIdeaIDKeys = generatedPsKeys.getInt(1);
			matches.setIdeaID(matchesIdeaIDKeys);
			// https://stackoverflow.com/questions/19873190/statement-getgeneratedkeys-method
			// https://stackoverflow.com/questions/1915166/how-to-get-the-insert-id-in-jdbc?noredirect=1&lq=1

			Integer tag_ID = checkTagNameExists(Tags);
			if (tag_ID == 0) {

				String inserttags = "INSERT INTO Tags (tagid, tagName) VALUES (?, ?)";
				PreparedStatement ps1;
				ps1 = con.prepareStatement(inserttags, Statement.RETURN_GENERATED_KEYS);
				ps1.setInt(1, tagID);
				ps1.setString(2, tags);
				ps1.executeUpdate();
				ResultSet generatedPs1Keys = ps1.getGeneratedKeys();
				generatedPs1Keys.next();
				int matchesTagIDKeys = generatedPs1Keys.getInt(1);
				matches.setTagID(matchesTagIDKeys);
			} else {
				matches.setTagID(tag_ID);
			}

			status = true;
			// https://en.wikipedia.org/wiki/Associative_entity
			String insertmatches = "INSERT INTO Matches (matchesid, ideaid, tagid) VALUES (?, ?, ?)";
			PreparedStatement ps3;
			ps3 = con.prepareStatement(insertmatches);
			ps3.setInt(1, matchesID);
			ps3.setInt(2, matches.getIdeaID());
			ps3.setInt(3, matches.getTagID());
			ps3.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
    			try {
    				if(con != null) con.close();
    			} catch(Exception e) {
        		e.printStackTrace();
        	}
    	}
		return status;
	}
}

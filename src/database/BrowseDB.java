package database;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import javax.sql.*;

import bean.Ideas;
import bean.SimilarProjects;

import javax.naming.*;
import java.util.*;

public class BrowseDB {

	Connection con;

	// Database configuration
	public static String url = "jdbc:mysql://localhost:3306/ideasincdb? ";
	public static String dbdriver = "com.mysql.jdbc.Driver";
	public static String username = "root";
	public static String password = "root";

	public BrowseDB() {
		try {
			Class.forName(dbdriver);
			con = DriverManager.getConnection(url, username, password);
			System.out.println("db connection ");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * public static void main(String[] args) { BrowseDB ob_DB_Connection=new
	 * BrowseDB(); System.out.println(ob_DB_Connection.con);
	 * ob_DB_Connection.searchInDBAjax("s"); }
	 */

	public List<Ideas> searchInDB(String keyword) {
		List<Ideas> ideaList = new ArrayList<Ideas>();
		try {
			String query = "Select * from ideas where ideaName like ? or description like ?";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, "%" + keyword + "%");
			ps.setString(2, "%" + keyword + "%");
			// System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Ideas idea = new Ideas(rs.getInt("ideaID"), rs.getString("ideaName"), rs.getString("description"),
						rs.getDate("datePublished"), rs.getInt("userID"));
				ideaList.add(idea);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// System.out.println(titleFound.get(0));
		return ideaList;
	}

	public Vector<String> searchInDBAjax(String keyword) {
		Vector<String> titleFound = new Vector<String>();
		try {
			String query = "Select * from ideas where ideaName like ?";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, "%" + keyword + "%");
			// System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				titleFound.addElement(rs.getString("ideaName"));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// System.out.println(titleFound);
		return titleFound;
	}

	 public int searchInDB(String username, String password) {
	    	int userId =  0;
	    	try {
	    		byte[] hash = null ;
	    		MessageDigest digest;
	    		String salt = password + "Spark";
				 StringBuilder sb = new StringBuilder();
	    		try {
	    		digest = MessageDigest.getInstance("SHA-256");
	    			hash = digest.digest(salt.getBytes(StandardCharsets.UTF_8));
	    		        for (byte b : hash) {
	    		            sb.append(String.format("%02x", b));
	    		        }

	    		} catch (NoSuchAlgorithmException e1) {
	    			// TODO Auto-generated catch block
	    			e1.printStackTrace();
	    		} 
	    		String query = "Select * from users where email = ? and password = ?";
	    		PreparedStatement ps = con.prepareStatement(query);
	    		ps.setString(1, username);
	    		ps.setString(2, sb.toString());
	    		ResultSet rs = ps.executeQuery();
//	    		System.out.println(ps);
	    		if(rs.next()) userId = rs.getInt("userId");;
	    	}
	    	catch(Exception e) {
	    		e.printStackTrace();
	    	}
	    	finally {
	    		try {
	    			if(con != null) con.close();
	    		}
	        	catch(Exception e) {
	        		e.printStackTrace();
	        	}
	    	}
	    	//System.out.println("found ? : " + userId);
	    	return userId;
	    }

	public List<Ideas> getAllIdeas() {
		ArrayList<Ideas> ideasList = new ArrayList<Ideas>();
		try {
			String query = "Select * from ideas";
			PreparedStatement ps = con.prepareStatement(query);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Ideas idea = new Ideas(rs.getInt("ideaID"), rs.getString("ideaName"), rs.getString("description"),
						rs.getDate("datePublished"), rs.getInt("userID"));
				ideasList.add(idea);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ideasList;
	}

	public Ideas getIdeaByID(String strIdeaID) {
		Ideas ideaObj = new Ideas();
		try {
			int ideaID = Integer.parseInt(strIdeaID);
			String query = "Select * from ideas where ideaID = ?";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setInt(1, ideaID);
			ResultSet rs = ps.executeQuery();
			System.out.println(ps);
			if (rs.next())
				ideaObj.setIdeaID(rs.getInt(1));
			ideaObj.setIdeaName(rs.getString(2));
			ideaObj.setDescription(rs.getString(3));
			ideaObj.setDatePublished(rs.getDate(4));
			ideaObj.setUserID(rs.getInt(5));

			System.out.println(ideaObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ideaObj;
	}

	// Populate browse.jsp when initializing
	public String searchInDB(int sl) {
		String title = null;
		try {
			String query = "Select * from ideas where ideaID = ?";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setInt(1, sl);
			ResultSet rs = ps.executeQuery();
			System.out.println(ps);
			if (rs.next())
				title = rs.getString("ideaName");
			System.out.println(title);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return title;
	}

	// Populate ideaContent.jsp when initializing
	public String searchInDB(String title, boolean contentOnly) {
		String content = "";
		try {
			String query = "Select * from ideas where ideaName = ?";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, title);
			ResultSet rs = ps.executeQuery();
			System.out.println(ps);
			if (rs.next())
				content = rs.getString("description");
			// System.out.println(content);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// System.out.println(content);
		return content;
	}

	// Populate ideaContent.jsp when initializing
	public String searchForTag(String title, boolean contentOnly) {
		String content = "";
		try {
			String query = "select tagName from Ideas i, Matches m, Tags t where i.ideaID = m.ideaID and \r\n"
					+ "t.tagID = m.tagID and i.ideaID = (select ideaID from Ideas i2 where \r\n" + "i2.ideaName = ?);";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, title);
			ResultSet rs = ps.executeQuery();
			System.out.println(ps);
			if (rs.next())
				content = rs.getString("tagName");
			// System.out.println(content);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// System.out.println(content);
		return content;
	}
}

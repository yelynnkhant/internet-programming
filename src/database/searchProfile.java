package database;

import java.sql.*;
import java.util.Vector;


public class searchProfile {
    Connection con;
    
    // Database configuration
    public static String url = "jdbc:mysql://localhost:3306/ideasincdb";
    public static String dbdriver = "com.mysql.jdbc.Driver";
    public static String username = "root";
    public static String password = "root";
    
    public searchProfile() {
		try {
			Class.forName(dbdriver);
			con = DriverManager.getConnection(url,username,password);
			System.out.println("db connection ");
	 	} catch (Exception e) {
			e.printStackTrace();
	 	}
    }
    
    public Vector<String> searchInDBAjax(String keyword) {
    	Vector<String> nameFound = new Vector<String>();
    	try {
	    	String query = "Select * from users where firstName like ? or lastName like ?";
	    	PreparedStatement ps = con.prepareStatement(query);
	    	ps.setString(1, "%" +  keyword + "%");
	    	ps.setString(2, "%" +  keyword + "%");
	    	//System.out.println(ps);
	    	ResultSet rs = ps.executeQuery();
	    	while(rs.next()) nameFound.addElement(rs.getString("userID") + ",," + rs.getString("firstName") + ",," + rs.getString("lastName") + ",," + rs.getString("email")); 
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
    	finally {
    		try {
    			if(con != null) con.close();
    		}
        	catch(Exception e) {
        		e.printStackTrace();
        	}
    	}
    	//System.out.println(titleFound);
    	return nameFound;
    }

}

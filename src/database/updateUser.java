package database;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.Users;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Servlet implementation class updateUser
 */
@WebServlet("/updateUser")
public class updateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Connection con;
    public static String url = "jdbc:mysql://localhost:3306/ideasincdb";
    public static String dbdriver = "com.mysql.jdbc.Driver";
    public static String username = "root";
    public static String password = "root";
    
    String title = "";
    String Fname  = "";
    String Lname = "";
    String email  = "";
    String skills  = "";
    String occupation  = "";
    String country  = "";
    String pass = "";
    Integer usID;
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public updateUser() {
        super();
        // TODO Auto-generated constructor stub
        
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("post received");
		// TODO Auto-generated method stub
//		myuser.setSalutations(request.getParameter("title"));
		title = request.getParameter("title");
		System.out.println(title);
		Fname = request.getParameter("firstname");
		Lname = request.getParameter("lastname");
		email = request.getParameter("email");
		skills = request.getParameter("skills");
		occupation = request.getParameter("occupation");
		country = request.getParameter("country");
		pass = request.getParameter("password");
		HttpSession session = request.getSession(true);
		usID = (Integer) session.getAttribute("userId");
		
		try {
			Class.forName(dbdriver);
			con = DriverManager.getConnection(url,username,password);
			System.out.println("Connection Successful");
	 	} catch (Exception e) {
			e.printStackTrace();
	 	}

		
		try {
			PreparedStatement ps;
			if(pass == "") {
				String query = "UPDATE users SET salutations=?,firstName=?,lastName=?,email=?,occupation=?,skills=?,location=? WHERE userID=?";
		    	ps = con.prepareStatement(query);
		    	ps.setString(1, title);
		    	ps.setString(2, Fname);
		    	ps.setString(3, Lname);
		    	ps.setString(4, email);
		    	ps.setString(5, occupation);
		    	ps.setString(6, skills);
		    	ps.setString(7, country);	    	
		    	ps.setInt(8, usID);
			}
			else {
				byte[] hash = null ;
	    		MessageDigest digest;
	    		String salt = pass + "Spark";
				 StringBuilder sb = new StringBuilder();
	    		try {
	    		digest = MessageDigest.getInstance("SHA-256");
	    			hash = digest.digest(salt.getBytes(StandardCharsets.UTF_8));
	    		        for (byte b : hash) {
	    		            sb.append(String.format("%02x", b));
	    		        }

	    		} catch (NoSuchAlgorithmException e1) {
	    			// TODO Auto-generated catch block
	    			e1.printStackTrace();
	    		} 
	    		
	    		
				String query = "UPDATE users SET salutations=?,firstName=?,lastName=?,email=?,password=?,occupation=?,skills=?,location=? WHERE userID=?";
		    	ps = con.prepareStatement(query);
		    	ps.setString(1, title);
		    	ps.setString(2, Fname);
		    	ps.setString(3, Lname);
		    	ps.setString(4, email);
		    	ps.setString(5, sb.toString());
		    	ps.setString(6, occupation);
		    	ps.setString(7, skills);
		    	ps.setString(8, country);	    	
		    	ps.setInt(9, usID);
			}

	    	System.out.println(ps);
	    	ps.executeUpdate();
	    	System.out.println("updated");
	    	response.sendRedirect("editProfile.jsp?info=success");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
    		try {
    			if(con != null) con.close();
    		}
        	catch(Exception e) {
        		e.printStackTrace();
        	}
    	}
		
	}

}

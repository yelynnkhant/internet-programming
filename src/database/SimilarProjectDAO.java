package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import bean.SimilarProjects;
import bean.Users;

public class SimilarProjectDAO {
	Connection con;

	public SimilarProjectDAO() {

	}

	public boolean insertSimilarProject(SimilarProjects similarProject) {
		int flag = 0;

		con = DBConnection.createConnection();
		String insertStatement = "insert into SimilarProjects(titleName, url, description,"
				+ " datePosted, userID, ideaID) values (?, ?, ?, now(), ?, ?)";

		try {
//			Statement statement = con.createStatement();
//			statement.executeUpdate(insertStatement);

			PreparedStatement statement = con.prepareStatement(insertStatement);
			statement.setString(1, similarProject.getTitleName());
			statement.setString(2, similarProject.getUrl());
			statement.setString(3, similarProject.getDescription());
//    		statement.setTimestamp(4, similarProject.getDataPosted());
			statement.setInt(4, similarProject.getUserID());
			statement.setInt(5, similarProject.getIdeaID());
			flag = statement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if(con != null) con.close();
			} catch(Exception e) {
    		e.printStackTrace();
    	}
	}
		
		return flag > 0;

	}

	public List<SimilarProjects> getSimilarProjectsByIdeaID(String strIdeaID) throws Exception {
		con = DBConnection.createConnection();
		List<SimilarProjects> similarProjects = new ArrayList<SimilarProjects>();
		String queryStatement = "select titleID, titleName, url, description, s.datePosted, "
				+ "s.userID, ideaID, salutations, firstName, lastName from SimilarProjects s, Users u where s.userID = u.userID and ideaID = ? order by titleID desc";
		PreparedStatement statement;
		int ideaID=Integer.parseInt(strIdeaID);
		System.out.println(ideaID);
		if(ideaID != 0) {

			try {
				statement = con.prepareStatement(queryStatement);
				statement.setInt(1, ideaID);
				ResultSet resultSet = statement.executeQuery();
				System.out.println(resultSet);
				Users user;
				while (resultSet.next()) {
					SimilarProjects project = new SimilarProjects();
					project.setTitleID(resultSet.getInt("titleID"));
					project.setTitleName(resultSet.getString("titleName"));
					project.setUrl(resultSet.getString("url"));
					project.setDescription(resultSet.getString("description"));
					project.setDataPosted(resultSet.getTimestamp("s.datePosted"));
					project.setUserID(resultSet.getInt("s.userID"));
					project.setIdeaID(resultSet.getInt("ideaID"));
					user = new Users();
					user.setSalutations(resultSet.getString("salutations"));
					user.setFirstName(resultSet.getString("firstName"));
					user.setLastName(resultSet.getString("lastName"));
					project.setUser(user);
					similarProjects.add(project);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
			    System.out.println("Exception in " + e);
	            throw new SQLException("Couldn't open connection to database: " +
	                    e.getMessage());
			}	
		}
		return similarProjects;
	}

	public Integer findIdeaIdByname(String name) {
		con = DBConnection.createConnection();
		String queryStatement = "select ideaID from Ideas where ideaName = ? limit 1";
		PreparedStatement statement;
		try {
			statement = con.prepareStatement(queryStatement);
			statement.setString(1, name);
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				return resultSet.getInt("ideaID");
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
		return 0;
		
	}
	
//	public boolean insertQuickUserSetup(Users user) {
//		int flag = 0;
//		String insertStatement = "insert into Users(salutations, firstName, lastName, email, password"
//				+ ") values (?, ?, ?, ?, ?)";
//		try {
////			Statement statement = con.createStatement();
////			statement.executeUpdate(insertStatement);
//
//			PreparedStatement statement = con.prepareStatement(insertStatement);
//			statement.setString(1, user.getSalutations());
//			statement.setString(2, user.getFirstName());
//			statement.setString(3, user.getLastName());
//			statement.setString(4, user.getEmail());
//			statement.setString(5, user.getPassword());
//			flag = statement.executeUpdate();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		return flag > 0;
//
//	}
}

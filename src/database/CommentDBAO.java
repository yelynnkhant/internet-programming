package database;

import java.sql.*;
import javax.sql.*;

import com.sun.org.apache.bcel.internal.generic.NEW;

import bean.Comments;

import javax.naming.*;
import java.util.*;
import exception.*;

// The instance of BookDBAO gets created when the application
// is deployed. It maintains the Connection object to the
// database. The Connection object is created from DataSource
// object, which is retrieved through JNDI.
// For more information on DataSource, please see
// http://java.sun.com/j2se/1.4.2/docs/api/javax/sql/DataSource.html.
public class CommentDBAO {
	private List<Comments> comments;
	Connection con;
	private boolean conFree = true;

	// Database configuration
	public static String url = "jdbc:mysql://localhost:3306/ideasincdb";
	public static String dbdriver = "com.mysql.jdbc.Driver";
	public static String username = "root";
	public static String password = "root";

	public CommentDBAO() throws Exception {
		try {
			/*
			 * Context initCtx = new InitialContext(); Context envCtx = (Context)
			 * initCtx.lookup("java:comp/env"); DataSource ds = (DataSource)
			 * envCtx.lookup("jdbc/BookDB"); con = ds.getConnection();
			 */
			Class.forName(dbdriver);
			con = DriverManager.getConnection(url, username, password);

		} catch (Exception ex) {
			System.out.println("Exception in CommentDBAO: " + ex);
			throw new Exception("Couldn't open connection to database: " + ex.getMessage());
		}
	}

	public void remove() {
		try {
			con.close();
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
	}

	protected synchronized Connection getConnection() {
		while (conFree == false) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}

		conFree = false;
		notify();

		return con;
	}

	protected synchronized void releaseConnection() {
		while (conFree == true) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}

		conFree = true;
		notify();
	}

	public List<Comments> getComments(String strIdeaID) throws CommentsNotFoundException {
		comments = new ArrayList<Comments>();

		try {
			int ideaID=Integer.parseInt(strIdeaID);
			String selectStatement = "select * " + "from comments as c JOIN users as u ON c.userID = u.userID where ideaID= ?";
			getConnection();

			PreparedStatement prepStmt = con.prepareStatement(selectStatement);
			prepStmt.setInt(1, ideaID);
			ResultSet rs = prepStmt.executeQuery();

			while (rs.next()) {
				Comments ct = new Comments(rs.getString("comment"),
						rs.getTimestamp("datePosted"), rs.getInt("userID"), rs.getInt("ideaID"));
				ct.setLastName(rs.getString("lastName"));;
				ct.setSalutations(rs.getString("salutations"));;
				comments.add(ct);

			}

			prepStmt.close();
		} catch (SQLException ex) {
			throw new CommentsNotFoundException("Couldn't find ideaID: " + strIdeaID + " " + ex.getMessage());
		}

		releaseConnection();
		Collections.sort(comments);

		return comments;
	}

	public void addComment(Comments comment) throws CommentNotCreatedException {
		try {
			String insertCmt = "INSERT INTO comments(comment,datePosted,ideaID,userID)" + "VALUES(?,?,?,?)";
			getConnection();

			PreparedStatement prepStmt = con.prepareStatement(insertCmt);
			prepStmt.setString(1, comment.getComment());
			prepStmt.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
			prepStmt.setInt(3, comment.getIdeaID());
			prepStmt.setInt(4, comment.getUserID());

			int rowAffected = prepStmt.executeUpdate();

			if (rowAffected == 1) {

				prepStmt.close();
				releaseConnection();

			}
		} catch (SQLException ex) {
			releaseConnection();
			throw new CommentNotCreatedException(
					"Couldn't create comment for: " + comment.getIdeaID() + " " + ex.getMessage());
		}
	}
}

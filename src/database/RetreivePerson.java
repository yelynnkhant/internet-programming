package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;



/**
 * Servlet implementation class RetreivePerson
 */
@WebServlet("/RetreivePerson")
public class RetreivePerson extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Connection con;
	   
    // Database configuration
    public static String url = "jdbc:mysql://localhost:3306/ideasincdb";
    public static String dbdriver = "com.mysql.jdbc.Driver";
    public static String username = "root";
    public static String password = "root";
    ResultSet rs;
    
    String title = "";
    String Fname  = "";
    String Lname = "";
    String email  = "";
    String skills  = "";
    String occupation  = "";
    String country  = "";
    String idea = "";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RetreivePerson(Integer usID) {
        super();
        // TODO Auto-generated constructor stub
        try {
			Class.forName(dbdriver);
			con = DriverManager.getConnection(url,username,password);
			System.out.println("Connection Successful");
	 	} catch (Exception e) {
			e.printStackTrace();
	 	}
       
		//String usID = "1"; //to be changed from session
		
    	try {
	    	String query = "SELECT * FROM users WHERE userID=?";
	    	PreparedStatement ps = con.prepareStatement(query);
	    	ps.setInt(1, usID);
	    	System.out.println(ps);
	    	rs = ps.executeQuery();
	    	while(rs.next()) {
	    		title = rs.getString("salutations");
	    		Fname = rs.getString("firstName");
	    		Lname = rs.getString("lastName");
	    		email = rs.getString("email");
	    		skills = rs.getString("skills");
	    		occupation = rs.getString("occupation");
	    		country = rs.getString("location");
	    	}
    	}
    	catch(Exception e) {
    		System.out.println("user table error");
    	}
    	
    	try {
    		String query = "SELECT * FROM ideas WHERE userID=?";
    		PreparedStatement ps = con.prepareStatement(query);
	    	ps.setInt(1, usID);
	    	rs = ps.executeQuery();
	    	while(rs.next()) {
	    		idea += ("<a href=ideadetail?ideaID=" + rs.getString("ideaID") + ">" + rs.getString("ideaname") + "</a><br/>");
	    	}
    	}
    	catch(Exception e){
    		System.out.println("idea table error");
    	}
    	finally {
    		try {
    			if(con != null) con.close();
    		}
        	catch(Exception e) {
        		e.printStackTrace();
        	}
    	}
    }
    public String getTitle() {
    	System.out.println(title);
    	return title;
    }
    
    public String getFName() {
    	System.out.println(Fname);
    	return Fname;
    }
    
    public String getLName() {
    	System.out.println(Lname);
    	return Lname;
    }
    
    public String getEmail(){
    	System.out.println(email);
    	return email;
    }
    
    public String getSkills(){
    	System.out.println(skills);
    	return skills;
    }
    
    public String getOccupation(){
    	System.out.println(occupation);
    	return occupation;
    }
    
    public String getCountry(){
    	System.out.println(country);
    	return country;
    }
    
    public String getIdeas() {
    	System.out.println(idea);
    	return idea;
    }

}
